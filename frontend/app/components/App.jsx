import React from 'react';
import Notes from './Notes.jsx';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Notes />
        <img src={require('../resources/marvin.jpg')}></img>
      </div>
    );
  }
}